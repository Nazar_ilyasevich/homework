<?php
if (!isset($_POST['submit'])){
    header('location:http://localhost:8888');
}
function dd(...$vars): void{
    echo "<pre>";
    var_dump($vars);
    echo "</pre>";
    exit();
}



$result = file_put_contents('data.txt', json_encode($_POST).PHP_EOL, FILE_APPEND);


if ($result){
    echo '<p>successful</p>';
    echo '<a href="http://lesson/">home</a><br>';
    echo '<a href="http://lesson/admin.php">admin</a>';
    return;
}

echo '<p class="uppercase tracking-wide text-gray-700 text-xl font-bold mb-2">error</p>';
