<?php

function dd(...$vars): void{
    echo "<pre>";
    var_dump($vars);
    echo "</pre>";
    exit();
}
function checkLength(string $string): string{
    if (mb_strlen($string) > 30){
        return substr($string, 0, 30) . '...';
    }elseif (empty($string)){
        return '***';
    }
    return $string;
}


$data = file('data.txt');


$dataDecode = array_map(function ($json){
    $data = json_decode($json, true);
    unset($data['submit']);
    return $data;
}, $data);

$keys = array_keys($dataDecode[0])


//dd(array_keys($dataDecode[0]));
?>




<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdn.tailwindcss.com"></script>
    <title>Document</title>
</head>
<body>
<div class="grid h-screen place-items-center">
    <div class="relative overflow-x-auto">
        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th class="px-6 py-3">ID</th>
                    <?php foreach ($keys as $key): ?>
                        <th class="px-6 py-3"><?= $key ?></th>
                    <?php endforeach; ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($dataDecode as $key => $lineData): ?>
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"><?= $key ?></th>
                        <?php foreach ($lineData as $value): ?>
                            <td class="px-6 py-4"><?= checkLength($value); ?></td>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
